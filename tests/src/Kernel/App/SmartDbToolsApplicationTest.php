<?php

declare(strict_types=1);

namespace Drupal\Tests\smart_db_tools\Kernel\App;

use Drupal\Core\Command\DbImportCommand;
use Drupal\smart_db_tools\App\SmartDbToolsApplication;
use Drupal\smart_db_tools\Command\SmartDbDumpCommand;
use Drupal\Tests\system\Kernel\Scripts\DbToolsApplicationTest;

/**
 * Test that the SmartDbToolsApplication works correctly.
 *
 * @group smart_db_tools
 * @group console
 *
 * @see \Drupal\Tests\system\Kernel\Scripts\DbToolsApplicationTest
 */
class SmartDbToolsApplicationTest extends DbToolsApplicationTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'smart_db_tools',
  ];

  /**
   * {@inheritdoc}
   */
  public function testDumpCommandRegistration(): void {
    $application = new SmartDbToolsApplication();
    $command = $application->find('dump');
    $this->assertInstanceOf(SmartDbDumpCommand::class, $command);
    $this->assertSame(\Drupal::VERSION, $application->getVersion());
  }

  /**
   * {@inheritdoc}
   */
  public function testImportCommandRegistration(): void {
    $application = new SmartDbToolsApplication();
    $command = $application->find('import');
    $this->assertInstanceOf(DbImportCommand::class, $command);
  }

}
