<?php

declare(strict_types=1);

namespace Drupal\Tests\smart_db_tools\Kernel\mysql\Console;

use Drupal\smart_db_tools\Command\SmartDbDumpCommand;
use Drupal\Tests\mysql\Kernel\mysql\Console\DbDumpCommandTest;
use PHPUnit\Framework\SkippedTestError;
use PHPUnit\Framework\SkippedWithMessageException;
use PHPUnit\Framework\SyntheticSkippedError;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Tests that SmartDbDumpCommand works correctly.
 *
 * @group smart_db_tools
 * @group console
 *
 * @see \Drupal\Tests\system\Kernel\Scripts\DbDumpCommandTest
 */
class SmartDbDumpCommandTest extends DbDumpCommandTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'smart_db_tools',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void /* @phpstan-ignore-line */ {
    // Parent::setup() throws an exception because of the namespace of the DB
    // driver.
    try {
      parent::setUp();
    }
    // @phpstan-ignore-next-line
    catch (SkippedWithMessageException | SyntheticSkippedError | SkippedTestError $e) {
      if (!str_contains($e->getMessage(), "'smart_db_tools'")) {
        throw $e;
      }
    }

    // Setup from the parent class.
    // Rebuild the router to ensure a routing table.
    \Drupal::service('router.builder')->rebuild();

    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = $this->container->get('database');
    $connection->insert('router')->fields(['name', 'path', 'pattern_outline'])->values(['test', 'test', 'test'])->execute();

    // Create a table with a field type not defined in
    // \Drupal\Core\Database\Schema::getFieldTypeMap.
    $table_name = $connection->getPrefix() . 'foo';
    $sql = "create table if not exists `$table_name` (`test` datetime NOT NULL);";
    $connection->query($sql)->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function testDbDumpCommand(): void {
    $command = new SmartDbDumpCommand();
    $command_tester = new CommandTester($command);
    $command_tester->execute([]);

    // Assert that insert exists and that some expected fields exist.
    $output = $command_tester->getDisplay();
    $this->assertStringContainsString("createTable('router", $output, 'Table router found');
    $this->assertStringContainsString("insert('router", $output, 'Insert found');
    $this->assertStringContainsString("'name' => 'test", $output, 'Insert name field found');
    $this->assertStringContainsString("'path' => 'test", $output, 'Insert path field found');
    $this->assertStringContainsString("'pattern_outline' => 'test", $output, 'Insert pattern_outline field found');
    $version = \Drupal::VERSION;
    $version_normalized = preg_replace(
      [
        '/\.x-dev$/',
        '/\.(\d+)-dev$/',
      ],
      [
        '.0',
        '.$1',
      ],
      $version
    );
    if (version_compare($version_normalized, '9.2', 'ge')) {
      $this->assertStringContainsString("// phpcs:ignoreFile", $output);
    }
    else {
      $this->assertStringContainsString("// @codingStandardsIgnoreFile", $output);
    }

    $this->assertStringContainsString("This file was generated by the Drupal {$version} db-tools.php script.", $output);
  }

  /**
   * {@inheritdoc}
   */
  public function testSchemaOnly(): void {
    $command = new SmartDbDumpCommand();
    $command_tester = new CommandTester($command);
    $command_tester->execute(['--schema-only' => 'router']);

    // Assert that insert statement doesn't exist for schema only table.
    $output = $command_tester->getDisplay();
    $this->assertStringContainsString("createTable('router", $output, 'Table router found');
    $this->assertStringNotContainsString("insert('router", $output, 'Insert not found');
    $this->assertStringNotContainsString("'name' => 'test", $output, 'Insert name field not found');
    $this->assertStringNotContainsString("'path' => 'test", $output, 'Insert path field not found');
    $this->assertStringNotContainsString("'pattern_outline' => 'test", $output, 'Insert pattern_outline field not found');

    // Assert that insert statement doesn't exist for wildcard schema only
    // match.
    $command_tester->execute(['--schema-only' => 'route.*']);
    $output = $command_tester->getDisplay();
    $this->assertStringContainsString("createTable('router", $output, 'Table router found');
    $this->assertStringNotContainsString("insert('router", $output, 'Insert not found');
    $this->assertStringNotContainsString("'name' => 'test", $output, 'Insert name field not found');
    $this->assertStringNotContainsString("'path' => 'test", $output, 'Insert path field not found');
    $this->assertStringNotContainsString("'pattern_outline' => 'test", $output, 'Insert pattern_outline field not found');
  }

}
