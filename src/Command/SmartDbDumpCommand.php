<?php

declare(strict_types=1);

namespace Drupal\smart_db_tools\Command;

use Drupal\Component\Utility\Variable;
use Drupal\Core\Command\DbDumpCommand;
use Drupal\Core\Database\Connection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * An improved command to dump the current database to a PHP script.
 *
 * This command allows users to create per-table-split database dump script.
 */
class SmartDbDumpCommand extends DbDumpCommand {

  /**
   * Default maximal number of items in a single file.
   *
   * @const int
   */
  const MAX_ITEMS_DEFAULT = 1000;

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this->addOption('split-destination', NULL, InputOption::VALUE_OPTIONAL, 'The destination file if per-table-split dump should be generated.', NULL);
    $this->addOption('subsplit-limit', NULL, InputOption::VALUE_OPTIONAL, 'Maximal number of items in a single file. Set to 0 to disable table subsplits.', self::MAX_ITEMS_DEFAULT);
    parent::configure();
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output): int {
    if ($destination = $input->getOption('split-destination')) {
      $connection = $this->getDatabaseConnection($input);
      $schema_tables = $input->getOption('schema-only');
      $schema_tables = explode(',', $schema_tables);
      $filesystem = new Filesystem();
      $destination_directory = dirname($destination) . DIRECTORY_SEPARATOR . pathinfo($destination, PATHINFO_FILENAME);
      if (!$filesystem->exists($destination_directory)) {
        $filesystem->mkdir([$destination_directory]);
      }

      $split_limit = (int) $input->getOption('subsplit-limit');
      $insert_count = (int) $input->getOption('insert-count');
      $this->generateSplitScripts($connection, $schema_tables, $destination, $split_limit, $insert_count);
      return 0;
    }

    return parent::execute($input, $output);
  }

  /**
   * Generates the database script.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection to use.
   * @param array $schema_only
   *   Table patterns for which to only dump the schema, no data.
   * @param string $main_file_destination
   *   The destination of the main file.
   * @param int $subsplit_limit
   *   The maximal number of db records in a single file.
   * @param int $insert_count
   *   The maximal number of records to insert in a single statement.
   */
  protected function generateSplitScripts(Connection $connection, array $schema_only, string $main_file_destination, int $subsplit_limit, int $insert_count) :void {
    $tables = '';
    $filesystem = new Filesystem();
    $basename = pathinfo($main_file_destination, PATHINFO_FILENAME);
    $destination_base = dirname($main_file_destination) . DIRECTORY_SEPARATOR . $basename;
    // Substitute in the version.
    $script = str_replace('{{VERSION}}', \Drupal::VERSION, $this->getTemplate());

    $schema_only_patterns = [];
    foreach ($schema_only as $match) {
      $schema_only_patterns[] = '/^' . $match . '$/';
    }

    foreach ($this->getTables($connection) as $table) {
      $schema = $this->getTableSchema($connection, $table);
      $table_filename_base = $destination_base . DIRECTORY_SEPARATOR . $table;
      $table_template = NULL;

      // Schema-only tables only need a table to be created.
      if (!empty($schema_only_patterns) && !preg_replace($schema_only_patterns, '', $table)) {
        $table_template = str_replace('{{TABLES}}', trim($this->getTableScript($table, $schema, [], $insert_count)), $script);
      }
      else {
        $more_than_one_page = FALSE;
        $data = $this->getTableData($connection, $table, 0, $subsplit_limit, $more_than_one_page);

        if (!$more_than_one_page) {
          $table_template = str_replace('{{TABLES}}', trim($this->getTableScript($table, $schema, $data, $insert_count)), $script);
        }
        else {
          // If there are more than '$subsplit_limit' records in the table,
          // we will create more files for a single table.
          $next_page = FALSE;
          $page = 0;
          $include_lines = '';

          do {
            $sub_split = $this->getTableData($connection, $table, $page, $subsplit_limit, $next_page);
            $split_file_name = $table . '_' . $page . '.php';
            $split_file_path = $table_filename_base . DIRECTORY_SEPARATOR . $split_file_name;
            // If this is not the first item, we don't need the create table
            // statement.
            $with_create = empty($include_lines);
            $table_split_template = str_replace(
              '{{TABLES}}',
              trim($this->getTableScript($table, $schema, $sub_split, $insert_count, $with_create)),
              $script
            );

            $filesystem->dumpFile($split_file_path, $table_split_template);

            $include_lines .= "include '{$table}' . DIRECTORY_SEPARATOR . '{$split_file_name}';\n";
            $page++;
          } while ($next_page === TRUE);

          // Substitute in the main table script.
          $table_template = str_replace('{{TABLES}}', trim($include_lines), $script);
        }
      }

      $filename = $table_filename_base . '.php';
      $filesystem->dumpFile($filename, $table_template);
      $tables .= "include '$basename' . DIRECTORY_SEPARATOR . '{$table}.php';\n";
    }

    // Substitute in the tables.
    $script = str_replace('{{TABLES}}', trim($tables), $script);
    $filesystem->dumpFile($main_file_destination, trim($script));
  }

  /**
   * Gets all data from a given table.
   *
   * If a table is set to be schema only, and empty array is returned.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection to use.
   * @param string $table
   *   The table to query.
   * @param int $page
   *   Which page to return (LIMIT offset / limit).
   * @param int $range
   *   Max number of records to return.
   * @param mixed $next
   *   If there are more items, this variable will be changed to TRUE. If there
   *   aren't more items, it is changed to FALSE.
   *
   * @return array
   *   The data from the table as an array.
   */
  protected function getTableData(Connection $connection, $table, int $page = 0, int $range = 0, &$next = NULL) {
    if ($range <= 0) {
      $next = FALSE;
      return parent::getTableData($connection, $table);
    }

    $order = $this->getFieldOrder($connection, $table);
    $limit = ' LIMIT ' . $page * $range . ", {$range}";
    $query = $connection->query("SELECT * FROM {" . $table . "} " . $order . $limit);
    $results = [];
    while (($row = $query->fetchAssoc()) !== FALSE) {
      $results[] = $row;
    }

    // Check if there are more records in this table.
    $check_limit = ' LIMIT ' . ($page + 1) * $range . ", 1";
    $next = (bool) $connection->query("SELECT * FROM {" . $table . "} " . $order . $check_limit)->fetchAssoc();

    return $results;
  }

  /**
   * The part of the script for each table.
   *
   * @param string $table
   *   Table name.
   * @param array $schema
   *   Drupal schema definition.
   * @param array $data
   *   Data for the table.
   * @param int $insert_count
   *   The maximal number of records to insert in a single statement.
   * @param bool $with_create
   *   Whether the script should also create the table. Defaults to TRUE.
   *
   * @return string
   *   The table create statement, and if there is data, the insert command.
   */
  protected function getTableScript($table, array $schema, array $data, int $insert_count = 1000, bool $with_create = TRUE): string {
    if ($with_create) {
      return parent::getTableScript($table, $schema, $data, $insert_count);
    }
    $output = '';
    if (!empty($data)) {
      $insert = '';
      foreach ($data as $record) {
        $insert .= "->values(" . Variable::export($record) . ")\n";
      }
      $fields = Variable::export(array_keys($schema['fields']));
      $output .= <<<EOT
\$connection->insert('$table')
->fields($fields)
{$insert}->execute();

EOT;
    }
    return $output;
  }

}
