<?php

declare(strict_types=1);

namespace Drupal\smart_db_tools\App;

use Drupal\Core\Command\DbDumpCommand;
use Drupal\Core\Command\DbToolsApplication;
use Drupal\smart_db_tools\Command\SmartDbDumpCommand;

/**
 * Provides a command to import or dumb a database.
 */
class SmartDbToolsApplication extends DbToolsApplication {

  /**
   * {@inheritdoc}
   */
  protected function getDefaultCommands(): array {
    $default_commands = [];

    foreach (parent::getDefaultCommands() as $command) {
      if ($command instanceof DbDumpCommand) {
        continue;
      }
      $default_commands[] = $command;
    }

    $default_commands[] = new SmartDbDumpCommand();
    return $default_commands;
  }

}
