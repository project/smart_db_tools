#!/usr/bin/env php
<?php

/**
 * @file
 * A command line application to import or dump a database.
 */

use Drupal\Core\DrupalKernel;
use Drupal\Core\Site\Settings;
use Drupal\smart_db_tools\App\SmartDbToolsApplication;
use Symfony\Component\HttpFoundation\Request;

if (PHP_SAPI !== 'cli') {
  return;
}

// Bootstrap.
$dir = __DIR__;
$dir_parts = explode(DIRECTORY_SEPARATOR, $dir);
for ($i = count($dir_parts); $i > 1; $i--) {
  $current_dir_parts = array_slice($dir_parts, 0, $i);
  $current_dir = implode(DIRECTORY_SEPARATOR, $current_dir_parts);
  $provisioned_drupal_class = implode(DIRECTORY_SEPARATOR, [
    $current_dir,
    'core',
    'lib',
    'Drupal.php',
  ]);

  if (
    file_exists($provisioned_drupal_class) &&
    file_exists($current_dir . DIRECTORY_SEPARATOR . 'autoload.php')
  ) {
    $autoloader = require $current_dir . DIRECTORY_SEPARATOR . 'autoload.php';
    $request = Request::createFromGlobals();
    Settings::initialize($current_dir, DrupalKernel::findSitePath($request), $autoloader);
    DrupalKernel::createFromRequest($request, $autoloader, 'prod')->boot();

    // Run the database dump command.
    $application = new SmartDbToolsApplication();
    $application->run();

    break;
  }
}

throw new \RuntimeException(
  sprintf(
    "Cannot determine Drupal 8|9 root. The script's current location is '%s'. Maybe it isn't placed into a Drupal instance?",
    __DIR__
  )
);
