CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Maintainers


INTRODUCTION
------------

This module contains an enhanced version of the DbToolsApplication of Drupal
core. The application contains an improved dump command which allows users to
save a database dump into multiple PHP scripts – so each database table will
be exported in its own file.

This functionality greatly reduces the time spent cleaning changed database
fixture scripts.


REQUIREMENTS
------------

No hard dependencies, only Drupal core.


INSTALLATION
------------

You can install Smart DB Tools as you would normally install a
contributed Drupal 8 or 9 module.


CONFIGURATION
-------------

This module does not have any configuration option.


USAGE
-----

The application is backward-compatible with the
[DbToolsApplication in Drupal core](1). The only difference is that if the
`--split-destination` option is passed to the script, then instead of a single
file, per-table database PHP scripts will be generated. The value of the
`--split-destination` option will be location of the PHP database dump.

To lower memory consumption, tables with more than 1000 records are also
exported into multiple files. This option can be changed by providing a
`--subsplit-limit` set to the desired value. Using `--subsplit-limit 1000` works
well with PHP `memory_limit` set to 256M. Setting subsplit limit to a number
lower than 1 disables this behavior.

**Example**:

```
php modules/contrib/smart_db_tools/scripts/smart-db-tools.php dump\
 --database fixture_connection\
 --subsplit-limit 5000\
 --split-destination modules/custom/db-dump.php
```

**Notes**

I wasn't able to find the dedicated documentation of the DbToolsApplication in
core to refer to.
An [example usage of importing a fixture can be found here](2), the
[usage of the *original* dump command is here](3).

MAINTAINERS
-----------

* Zoltán Horváth (huzooka) - https://www.drupal.org/u/huzooka

[1]: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Command%21DbToolsApplication.php/class/DbToolsApplication
[2]: https://drupal.org/node/2583227#s-importing-data-from-the-fixture-to-your-testdatabase
[3]: https://drupal.org/node/2583227#s-make-the-changes-and-export-to-the-changes-to-the-fixture-file
